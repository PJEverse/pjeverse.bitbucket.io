var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var AdaptCommand = (function () {
    function AdaptCommand(game, button) {
        var _this = this;
        this.click = function () {
            _this.execute();
        };
        this.game = game;
        this.render(button);
    }
    AdaptCommand.prototype.render = function (buttonid) {
        var buttons = document.querySelector('#buttons');
        var button = document.createElement('button');
        button.id = buttonid;
        button.innerHTML = buttonid;
        button.addEventListener('click', this.click);
        buttons.appendChild(button);
    };
    AdaptCommand.prototype.execute = function () {
        console.log('NOT IMPLEMENTED!!! PLS DO');
    };
    return AdaptCommand;
}());
var AttackCommand = (function (_super) {
    __extends(AttackCommand, _super);
    function AttackCommand(game, attack) {
        return _super.call(this, game, attack) || this;
    }
    AttackCommand.prototype.execute = function () {
        this.game.out.println(this.game.player.DealDamage(this.game.player, this.game.currentRoom.hasEnemy));
        this.game.out.println('>');
        if (this.game.currentRoom.hasEnemy.CurrentHealth > 0) {
            this.game.out.println(this.game.player.TakeDamage(this.game.player, this.game.currentRoom.hasEnemy));
        }
        this.game.out.println('>');
        if (this.game.currentRoom.hasEnemy.burn == true) {
            this.game.player.CurrentHealth = this.game.player.CurrentHealth - 5;
            this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' set you on fire. It does 5 damage.');
        }
        var element = document.getElementById("enemystats");
        element.parentNode.removeChild(element);
        this.game.EnemyStats();
        if (this.game.player.CurrentHealth > 0) {
            if (this.game.currentRoom.hasEnemy.CurrentHealth <= 0) {
                this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' died.');
                this.game.player.Gold = this.game.currentRoom.hasEnemy.Gold + this.game.player.Gold;
                this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' dropped ' + this.game.currentRoom.hasEnemy.Gold + ' gold.');
                if (this.game.currentRoom.hasEnemy.Name == "Deathwing") {
                    this.game.out.println("You have defeated the boss! You Win!");
                    this.game.gameOver();
                }
                else {
                    this.game.currentRoom.hasEnemy = null;
                    var buttons = document.getElementById('buttons');
                    var element2 = document.getElementById("Attack");
                    element2.parentNode.removeChild(element2);
                    new GoCommand(this.game, 'North');
                    new GoCommand(this.game, 'East');
                    new GoCommand(this.game, 'South');
                    new GoCommand(this.game, 'West');
                    new RestCommand(this.game, 'Rest');
                }
            }
        }
        var pelement = document.getElementById("playerstats");
        pelement.parentNode.removeChild(pelement);
        this.game.PlayerStats();
    };
    return AttackCommand;
}(AdaptCommand));
var BackCommand = (function (_super) {
    __extends(BackCommand, _super);
    function BackCommand(game, back) {
        return _super.call(this, game, back) || this;
    }
    BackCommand.prototype.execute = function () {
        var i;
        for (i = 0; i < 5; i++) {
            var element = document.getElementById('Item');
            element.parentNode.removeChild(element);
        }
        var element2 = document.getElementById('Back');
        element2.parentNode.removeChild(element2);
        new GoCommand(this.game, 'North');
        new GoCommand(this.game, 'East');
        new GoCommand(this.game, 'South');
        new GoCommand(this.game, 'West');
        new RestCommand(this.game, 'Rest');
        new ShopCommand(this.game, 'Shop');
    };
    return BackCommand;
}(AdaptCommand));
var BuyCommand = (function (_super) {
    __extends(BuyCommand, _super);
    function BuyCommand(game, Item) {
        var _this = _super.call(this, game, Item.Name) || this;
        _this.Item = Item;
        return _this;
    }
    BuyCommand.prototype.execute = function () {
        if (this.Item.Name == 'Evasion' && this.game.player.Evasion >= 80) {
            this.game.out.println("You can't get any faster!");
        }
        else {
            if (this.game.player.Gold - this.Item.Gold < 0) {
                this.game.out.println("You don't have enough gold.");
            }
            else {
                this.game.player.MaxHealth = this.game.player.MaxHealth + this.Item.Health;
                this.game.player.Damage = this.game.player.Damage + this.Item.Damage;
                this.game.player.Armor = this.game.player.Armor + this.Item.Armor;
                this.game.player.Evasion = this.game.player.Evasion + this.Item.Evasion;
                this.game.player.Gold = this.game.player.Gold - this.Item.Gold;
                this.game.player.CurrentHealth = this.game.player.CurrentHealth + this.Item.Health;
                this.game.out.println('You bought a ' + this.Item.Desc);
                var element = document.getElementById("playerstats");
                element.parentNode.removeChild(element);
                this.game.PlayerStats();
            }
        }
    };
    BuyCommand.prototype.render = function (buttonid) {
        var buttons = document.querySelector('#buttons');
        var button = document.createElement('button');
        button.id = 'Item';
        button.innerHTML = buttonid;
        button.addEventListener('click', this.click);
        buttons.appendChild(button);
    };
    return BuyCommand;
}(AdaptCommand));
var Character = (function () {
    function Character(MaxHealth, Damage, Armor, Evasion, Gold) {
        this.MaxHealth = MaxHealth;
        this.CurrentHealth = MaxHealth;
        this.Damage = Damage;
        this.Armor = Armor;
        this.Evasion = Evasion;
        this.Gold = Gold;
    }
    Character.prototype.TakeDamage = function (Player, Enemy) {
        if (Math.random() * 100 > Player.Evasion) {
            if ((Enemy.Damage - Player.Armor) < 5) {
                Player.CurrentHealth = Player.CurrentHealth - 5;
                return Enemy.Name + ' did 5 damage to you.';
            }
            else {
                Player.CurrentHealth = Player.CurrentHealth - (Enemy.Damage - Player.Armor);
                return Enemy.Name + ' did ' + Enemy.Damage + ' damage to you. ' + 'Your armor blocked ' + Player.Armor + ' Damage.';
            }
        }
        else {
            return 'You avoided the attack.';
        }
    };
    Character.prototype.DealDamage = function (Player, Enemy) {
        if (Math.random() * 100 > Enemy.Evasion) {
            if ((Player.Damage - Enemy.Armor) < 5) {
                Enemy.CurrentHealth = Enemy.CurrentHealth - 5;
                return 'You did 5 Damage.';
            }
            else {
                Enemy.CurrentHealth = Enemy.CurrentHealth - (Player.Damage - Enemy.Armor);
                return 'You did ' + Player.Damage + ' Damage. Their armor blocked ' + Enemy.Armor + ' damage.';
            }
        }
        else {
            return Enemy.Name + ' avoided the attack.';
        }
    };
    return Character;
}());
var Enemy = (function (_super) {
    __extends(Enemy, _super);
    function Enemy(Name, Image, MaxHealth, Damage, Armor, Evasion, Gold, burn) {
        var _this = _super.call(this, MaxHealth, Damage, Armor, Evasion, Gold) || this;
        _this.Name = Name;
        _this.Image = Image;
        _this.burn = burn;
        return _this;
    }
    return Enemy;
}(Character));
var Game = (function () {
    function Game(output) {
        var _this = this;
        this._el = document.querySelector('#body');
        this.player = new Character(50, 10, 5, 30, 10);
        this._timeUp = false;
        this.eStatsUp = false;
        this.createRooms = function () {
            var shop = new Room("at the shop.", false);
            var shop2 = new Room("at the shop.", false);
            var outside = new Room("outside the main entrance of the cave.", false);
            var arrival = new Room("at the road where you came from.", false);
            var yard = new Room("in the yard.", false);
            var entrance = new Room("at the cave entrance.", false);
            var west1 = new Room("going west.", false);
            var west2 = new Room("in the water elementals' room.", false);
            var west3 = new Room("in the watery caverns.", false);
            var west4 = new Room("at the lake.", false);
            var water = new Room("almost drowning.", false);
            var east1 = new Room("going towards the east.", false);
            var east2 = new Room("in the rock golems' room.", false);
            var east3 = new Room("in the living stone hall.", false);
            var east4 = new Room("at the earthen pillars.", false);
            var earth = new Room("standing on shaking ground.", false);
            var depths = new Room("going into the fiery depths.", false);
            var fire = new Room("almost melting.", true);
            var core = new Room("in the molten core of the dungeon.", true);
            var phoenix = new Room("attacked by the phoenix.", true);
            var lava = new Room("in a very hot room. There is probably a very dangerous enemy to the west.", true);
            var boss = new Room("at the end. This is the final fight.", true);
            shop.setExits(null, null, outside, null);
            shop2.setExits(null, null, null, depths);
            outside.setExits(shop, yard, entrance, arrival);
            arrival.setExits(null, outside, null, null);
            yard.setExits(null, null, null, outside);
            entrance.setExits(outside, east1, null, west1);
            east1.setExits(null, east2, null, entrance);
            east2.setExits(null, null, east3, east1);
            east3.setExits(east2, null, east4, null);
            east4.setExits(east3, null, null, earth);
            west1.setExits(null, entrance, west2, null);
            west2.setExits(west1, null, west3, null);
            west3.setExits(west2, null, west4, null);
            west4.setExits(west3, water, null, null);
            earth.setExits(null, east4, depths, null);
            water.setExits(null, depths, null, west4);
            depths.setExits(earth, shop2, fire, water);
            fire.setExits(depths, null, core, null);
            core.setExits(fire, phoenix, null, lava);
            phoenix.setExits(null, null, null, core);
            lava.setExits(null, core, null, boss);
            boss.setExits(null, lava, null, null);
            yard.spawnEnemy('Chicken', 'img/chicken.jpg', 1, 4, 1, 75, 5, false);
            east2.spawnEnemy('Rock Golem', 'img/golem.jpg', 50, 5, 50, 0, 10, false);
            east4.spawnEnemy('Rock Troll', 'img/troll.jpg', 50, 15, 75, 0, 20, false);
            west2.spawnEnemy('Water Elemental', 'img/water.jpg', 75, 10, 0, 30, 10, false);
            west4.spawnEnemy('Lake Elemental', 'img/lake.jpg', 100, 20, 0, 30, 20, false);
            earth.spawnEnemy('Earth Guardian', 'img/earth.jpg', 50, 25, 100, 0, 30, false);
            water.spawnEnemy('Water Guardian', 'img/waterg.jpg', 125, 30, 0, 30, 30, false);
            fire.spawnEnemy('Fire Guardian', 'img/fire.jpg', 120, 50, 10, 30, 30, true);
            phoenix.spawnEnemy('Phoenix', 'img/phoenix.jpg', 150, 50, 20, 10, 50, true);
            boss.spawnEnemy('Deathwing', 'img/boss.jpg', 400, 60, 50, 0, 1000, true);
            _this.currentRoom = outside;
        };
        this.out = new Printer(output);
        this.isOn = true;
        this.createRooms();
        this.printWelcome();
        this.render();
        this.PlayerStats();
        this.loop();
    }
    Game.prototype.randomTime = function (min, max) {
        return Math.round(Math.random() * (max - min) + min);
    };
    Game.prototype.printWelcome = function () {
        this.out.println();
        this.out.println("Welcome to the Game!");
        this.out.println();
        this.out.println("You are " + this.currentRoom.description);
        this.out.print("Exits: ");
        if (this.currentRoom.northExit != null) {
            this.out.print("north ");
        }
        if (this.currentRoom.eastExit != null) {
            this.out.print("east ");
        }
        if (this.currentRoom.southExit != null) {
            this.out.print("south ");
        }
        if (this.currentRoom.westExit != null) {
            this.out.print("west ");
        }
        this.out.println();
        this.out.print(">");
    };
    Game.prototype.gameOver = function () {
        this.isOn = false;
        var element = document.getElementById("buttons");
        element.parentNode.removeChild(element);
        this.out.println("Thank you for playing.  Good bye.");
        this.out.println("Hit F5 to restart the game");
    };
    Game.prototype.render = function () {
        var buttons = document.createElement('div');
        buttons.className = 'buttons';
        buttons.id = 'buttons';
        this._el.appendChild(buttons);
        new MapCommand(this, 'Map');
        new GoCommand(this, 'North');
        new GoCommand(this, 'East');
        new GoCommand(this, 'South');
        new GoCommand(this, 'West');
        new RestCommand(this, 'Rest');
    };
    Game.prototype.PlayerStats = function () {
        var player = this.player;
        var PlayerStats = document.createElement('div');
        PlayerStats.className = 'div';
        PlayerStats.id = 'playerstats';
        this._el.appendChild(PlayerStats);
        PlayerStats.innerHTML = player.CurrentHealth + '/' + player.MaxHealth +
            ' HP</br>' + player.Damage + ' DMG</br>' + player.Armor + ' Armor</br>' + player.Evasion + '% Evasion</br>' + player.Gold + ' Gold';
    };
    Game.prototype.EnemyStats = function () {
        if (document.getElementById('Kaart') != null) {
            var element = document.getElementById('Kaart');
            element.parentNode.removeChild(element);
        }
        var enemy = this.currentRoom.hasEnemy;
        var EnemyStats = document.createElement('div');
        EnemyStats.className = 'enemystats';
        EnemyStats.id = 'enemystats';
        this._el.appendChild(EnemyStats);
        EnemyStats.innerHTML = '<img src=' + enemy.Image + '></img></br>' + enemy.Name + '</br>' + enemy.CurrentHealth + '/' + enemy.MaxHealth +
            ' HP</br>' + enemy.Damage + ' DMG</br>' + enemy.Armor + ' Armor</br>' + enemy.Evasion + '% Evasion';
        this.eStatsUp = true;
    };
    Game.prototype.loop = function () {
        var _this = this;
        var time = this.randomTime(200, 1000);
        if (this.eStatsUp == false) {
            if (this.currentRoom.hasEnemy != null) {
                this.EnemyStats();
                var element1 = document.getElementById("North");
                element1.parentNode.removeChild(element1);
                var element1 = document.getElementById("East");
                element1.parentNode.removeChild(element1);
                var element2 = document.getElementById("South");
                element2.parentNode.removeChild(element2);
                var element3 = document.getElementById("West");
                element3.parentNode.removeChild(element3);
                var element4 = document.getElementById("Rest");
                element4.parentNode.removeChild(element4);
                new AttackCommand(this, 'Attack');
                this.eStatsUp = true;
            }
        }
        else {
            if (this.currentRoom.hasEnemy == null) {
                var element = document.getElementById("enemystats");
                element.parentNode.removeChild(element);
                this.eStatsUp = false;
            }
        }
        if (this.player.CurrentHealth <= 0) {
            this.gameOver();
        }
        setTimeout(function () {
            if (!_this._timeUp)
                _this.loop();
        }, time);
    };
    return Game;
}());
var GoCommand = (function (_super) {
    __extends(GoCommand, _super);
    function GoCommand(game, direction) {
        var _this = _super.call(this, game, direction) || this;
        _this.direction = direction;
        return _this;
    }
    GoCommand.prototype.getNextRoom = function () {
        switch (this.direction) {
            case 'North': {
                return this.game.currentRoom.northExit;
            }
            case 'East': {
                return this.game.currentRoom.eastExit;
            }
            case 'South': {
                return this.game.currentRoom.southExit;
            }
            case 'West': {
                return this.game.currentRoom.westExit;
            }
            default:
                return null;
        }
    };
    GoCommand.prototype.execute = function () {
        var nextRoom = null;
        nextRoom = this.getNextRoom();
        if (nextRoom == null) {
            this.game.out.println("There is no door!");
        }
        else {
            this.game.currentRoom = nextRoom;
            this.game.out.println("You are " + this.game.currentRoom.description);
            if (this.game.currentRoom.burn == true) {
                this.game.out.println("You are on fire!");
                this.game.player.CurrentHealth = this.game.player.CurrentHealth - 3;
                var element = document.getElementById("playerstats");
                element.parentNode.removeChild(element);
                this.game.PlayerStats();
            }
            this.game.out.print("Exits: ");
            if (this.game.currentRoom.northExit != null) {
                this.game.out.print("north ");
            }
            if (this.game.currentRoom.eastExit != null) {
                this.game.out.print("east ");
            }
            if (this.game.currentRoom.southExit != null) {
                this.game.out.print("south ");
            }
            if (this.game.currentRoom.westExit != null) {
                this.game.out.print("west ");
            }
            this.game.out.println();
            this.game.out.print(">");
            if (this.game.currentRoom.description == 'at the shop.') {
                new ShopCommand(this.game, 'Shop');
            }
            else {
                var element = document.getElementById("Shop");
                if (element != null) {
                    element.parentNode.removeChild(element);
                }
            }
        }
    };
    return GoCommand;
}(AdaptCommand));
var Item = (function () {
    function Item(Name, Desc, Health, Damage, Armor, Evasion, Gold) {
        this.Name = Name;
        this.Desc = Desc;
        this.Health = Health;
        this.Damage = Damage;
        this.Armor = Armor;
        this.Evasion = Evasion;
        this.Gold = Gold;
    }
    return Item;
}());
var MapCommand = (function (_super) {
    __extends(MapCommand, _super);
    function MapCommand() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MapCommand.prototype.execute = function () {
        if (document.getElementById('Kaart') == null) {
            var Kaart = document.createElement('div');
            Kaart.id = 'Kaart';
            this.game._el.appendChild(Kaart);
            Kaart.innerHTML = '<img src= img/map.svg></img>';
        }
        else {
            var element = document.getElementById("Kaart");
            element.parentNode.removeChild(element);
        }
    };
    return MapCommand;
}(AdaptCommand));
var Printer = (function () {
    function Printer(output) {
        this.output = output;
    }
    Printer.prototype.print = function (text) {
        this.output.innerHTML += text;
    };
    Printer.prototype.println = function (text) {
        if (text === void 0) { text = ""; }
        this.print(text + "<br/>");
        this.output.scrollTop = this.output.scrollHeight;
    };
    return Printer;
}());
var RestCommand = (function (_super) {
    __extends(RestCommand, _super);
    function RestCommand(game, rest) {
        return _super.call(this, game, rest) || this;
    }
    RestCommand.prototype.execute = function () {
        if (this.game.currentRoom.burn == true) {
            this.game.out.println('You cannot rest in this burning room.');
        }
        else {
            if (this.game.player.CurrentHealth < this.game.player.MaxHealth) {
                this.game.player.CurrentHealth = this.game.player.MaxHealth;
                var pelement = document.getElementById("playerstats");
                pelement.parentNode.removeChild(pelement);
                this.game.PlayerStats();
            }
            else {
                this.game.out.println('You are well rested.');
            }
        }
    };
    return RestCommand;
}(AdaptCommand));
var Room = (function () {
    function Room(description, burn) {
        var _this = this;
        this.spawnEnemy = function (name, img, hp, dmg, ar, ev, gd, burn) {
            var enemy = new Enemy(name, img, hp, dmg, ar, ev, gd, burn);
            _this.hasEnemy = enemy;
        };
        this.description = description;
        this.hasEnemy = null;
        this.burn = burn;
    }
    Room.prototype.setExits = function (north, east, south, west) {
        if (north != null) {
            this.northExit = north;
        }
        if (east != null) {
            this.eastExit = east;
        }
        if (south != null) {
            this.southExit = south;
        }
        if (west != null) {
            this.westExit = west;
        }
    };
    return Room;
}());
var ShopCommand = (function (_super) {
    __extends(ShopCommand, _super);
    function ShopCommand(game, Shop) {
        var _this = _super.call(this, game, Shop) || this;
        _this.Items = [new Item('Damage', 'Damage potion', 0, 10, 0, 0, 5),
            new Item('Health', 'Health potion', 10, 0, 0, 0, 5), new Item('Armor', 'Iron skin potion', 0, 0, 5, 0, 5),
            new Item('Evasion', 'Potion of Swiftness', 0, 0, 0, 5, 5),
            new Item('Cannon', 'Glass Cannon', -10, 30, 0, -5, 10)];
        return _this;
    }
    ShopCommand.prototype.execute = function () {
        var element = document.getElementById("North");
        element.parentNode.removeChild(element);
        var element1 = document.getElementById("East");
        element1.parentNode.removeChild(element1);
        var element2 = document.getElementById("South");
        element2.parentNode.removeChild(element2);
        var element3 = document.getElementById("West");
        element3.parentNode.removeChild(element3);
        var element4 = document.getElementById("Rest");
        element4.parentNode.removeChild(element4);
        var element5 = document.getElementById("Shop");
        element5.parentNode.removeChild(element5);
        var i;
        for (i = 0; i < 5; i++) {
            new BuyCommand(this.game, this.Items[i]);
        }
        new BackCommand(this.game, 'Back');
        this.game.out.println('Welcome to the shop.');
        this.game.out.println('Prices:');
        this.game.out.println('Damage Potion: 5 Gold.');
        this.game.out.println('Health Potion: 5 Gold.');
        this.game.out.println('Armor Potion: 5 Gold.');
        this.game.out.println('Evasion Potion: 5 Gold.');
        this.game.out.println('Glass Cannon: 10 Gold.');
        this.game.out.println('Be careful with that cannon, please.');
    };
    return ShopCommand;
}(AdaptCommand));
//# sourceMappingURL=main.js.map