class ShopCommand extends AdaptCommand
{
    Items : Array<Item>;
    game : Game;

    constructor(game : Game, Shop : string)
    {
        super(game, Shop);
        this.Items = [new Item('Damage', 'Damage potion', 0, 10, 0, 0, 5), 
        new Item('Health', 'Health potion', 10, 0, 0, 0, 5), new Item('Armor', 'Iron skin potion', 0, 0, 5, 0, 5), 
        new Item('Evasion', 'Potion of Swiftness', 0, 0, 0, 5, 5),
        new Item('Cannon', 'Glass Cannon', -10, 30, 0, -5, 10)];
    }

    execute()
    {
        let element = document.getElementById("North");
        element.parentNode.removeChild(element);
        let element1 = document.getElementById("East");
        element1.parentNode.removeChild(element1);
        let element2 = document.getElementById("South");
        element2.parentNode.removeChild(element2);
        let element3 = document.getElementById("West");
        element3.parentNode.removeChild(element3);
        let element4 = document.getElementById("Rest");
        element4.parentNode.removeChild(element4);
        let element5 = document.getElementById("Shop");
        element5.parentNode.removeChild(element5); 
        let i:number;
        for(i=0; i<5; i++)
        {
            new BuyCommand(this.game, this.Items[i]);
        }
        new BackCommand(this.game, 'Back');
        this.game.out.println('Welcome to the shop.');
        this.game.out.println('Prices:');
        this.game.out.println('Damage Potion: 5 Gold.');
        this.game.out.println('Health Potion: 5 Gold.');
        this.game.out.println('Armor Potion: 5 Gold.');
        this.game.out.println('Evasion Potion: 5 Gold.');
        this.game.out.println('Glass Cannon: 10 Gold.');
        this.game.out.println('Be careful with that cannon, please.');
    }

}