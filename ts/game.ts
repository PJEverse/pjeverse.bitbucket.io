
class Game 
{
    out : Printer;
    currentRoom : Room;
    isOn : boolean;
    _el : Element = document.querySelector('#body');
    player : Character = new Character(50, 10, 5, 30, 10);
    private _timeUp : boolean = false;
    eStatsUp : boolean = false;
 
    /**
     * Create the game and initialise its internal map.
     */
    constructor(output: HTMLElement) 
    {
        this.out = new Printer(output);
        this.isOn = true;
        this.createRooms();
        this.printWelcome();
        this.render();
        this.PlayerStats();
        this.loop();
            
    }

    /**
     * Create all the rooms and link their exits together.
     */
    createRooms = () => 
    {
        // create the rooms
        let shop = new Room("at the shop.", false);
        let shop2 = new Room("at the shop.", false);
        let outside = new Room("outside the main entrance of the cave.", false);
        let arrival = new Room("at the road where you came from.", false);
        let yard = new Room("in the yard.", false);
        let entrance = new Room("at the cave entrance.", false);
        let west1 = new Room("going west.", false);
        let west2 = new Room("in the water elementals' room.", false);
        let west3 = new Room("in the watery caverns.", false);
        let west4 = new Room("at the lake.", false);
        let water = new Room("almost drowning.", false)
        let east1 = new Room("going towards the east.", false);
        let east2 = new Room("in the rock golems' room.", false);
        let east3 = new Room("in the living stone hall.", false);
        let east4 = new Room("at the earthen pillars.", false);
        let earth = new Room("standing on shaking ground.", false)
        let depths = new Room("going into the fiery depths.", false)
        let fire = new Room("almost melting.", true)
        let core = new Room("in the molten core of the dungeon.", true)
        let phoenix = new Room("attacked by the phoenix.", true)
        let lava = new Room("in a very hot room. There is probably a very dangerous enemy to the west.", true)
        let boss = new Room("at the end. This is the final fight.", true)


        // initialise room exits
        shop.setExits(null, null, outside, null);
        shop2.setExits(null, null, null, depths);
        outside.setExits(shop, yard, entrance, arrival);
        arrival.setExits(null, outside, null, null);
        yard.setExits(null, null, null, outside);
        entrance.setExits(outside, east1, null, west1);
        east1.setExits(null, east2, null, entrance);
        east2.setExits(null, null, east3, east1);
        east3.setExits(east2, null, east4, null);
        east4.setExits(east3, null, null, earth);
        west1.setExits(null, entrance, west2, null);
        west2.setExits(west1, null, west3, null);
        west3.setExits(west2, null, west4, null);
        west4.setExits(west3, water, null, null);
        earth.setExits(null, east4, depths, null);
        water.setExits(null, depths, null, west4);
        depths.setExits(earth, shop2, fire, water);
        fire.setExits(depths, null, core, null);
        core.setExits(fire, phoenix, null, lava);
        phoenix.setExits(null, null, null, core);
        lava.setExits(null, core, null, boss);
        boss.setExits(null, lava, null, null);
        yard.spawnEnemy('Chicken', 'img/chicken.jpg', 1, 4, 1, 75, 5, false);
        east2.spawnEnemy('Rock Golem', 'img/golem.jpg', 50, 5, 50, 0, 10, false);
        east4.spawnEnemy('Rock Troll', 'img/troll.jpg', 50, 15, 75, 0, 20, false);
        west2.spawnEnemy('Water Elemental', 'img/water.jpg', 75, 10, 0, 30, 10, false);
        west4.spawnEnemy('Lake Elemental', 'img/lake.jpg', 100, 20, 0, 30, 20, false);
        earth.spawnEnemy('Earth Guardian', 'img/earth.jpg', 50, 25, 100, 0, 30, false);
        water.spawnEnemy('Water Guardian', 'img/waterg.jpg', 125, 30, 0, 30, 30, false);
        fire.spawnEnemy('Fire Guardian', 'img/fire.jpg', 120, 50, 10, 30, 30, true);
        phoenix.spawnEnemy('Phoenix', 'img/phoenix.jpg', 150, 50, 20, 10, 50, true);
        boss.spawnEnemy('Deathwing', 'img/boss.jpg', 400, 60, 50, 0, 1000, true);

        // spawn player outside
        this.currentRoom = outside;
    }

    private randomTime(min : number, max : number) : number 
    {
        return Math.round(Math.random() * (max - min) + min);
    }

 
    /**
     * Print out the opening message for the player.
     */
    
    printWelcome() : void 
    {
        this.out.println();
        this.out.println("Welcome to the Game!");
        this.out.println();
        this.out.println("You are " + this.currentRoom.description);
        this.out.print("Exits: ");
        if(this.currentRoom.northExit != null) {
            this.out.print("north ");
        }
        if(this.currentRoom.eastExit != null) {
            this.out.print("east ");
        }
        if(this.currentRoom.southExit != null) {
            this.out.print("south ");
        }
        if(this.currentRoom.westExit != null) {
            this.out.print("west ");
        }
        this.out.println();
        this.out.print(">");
    }

    gameOver() : void 
    {
        this.isOn = false;
        let element = document.getElementById("buttons");
            element.parentNode.removeChild(element);
        this.out.println("Thank you for playing.  Good bye.");
        this.out.println("Hit F5 to restart the game");
    }

    private render() : void 
    {
    const buttons = document.createElement('div');
        buttons.className = 'buttons';
        buttons.id = 'buttons';
        this._el.appendChild(buttons);
        new MapCommand(this, 'Map');
        new GoCommand(this, 'North');
        new GoCommand(this, 'East');
        new GoCommand(this, 'South');
        new GoCommand(this, 'West');
        new RestCommand(this, 'Rest');

    }

    PlayerStats()   
    {
        const player = this.player;
        const PlayerStats = document.createElement('div');
        PlayerStats.className = 'div';
        PlayerStats.id = 'playerstats';
        this._el.appendChild(PlayerStats);
        PlayerStats.innerHTML = player.CurrentHealth + '/' + player.MaxHealth + 
        ' HP</br>' + player.Damage + ' DMG</br>' + player.Armor + ' Armor</br>' + player.Evasion + '% Evasion</br>' + player.Gold + ' Gold';
    }
    EnemyStats()   
    {
        if(document.getElementById('Kaart') != null)
        {
        let element = document.getElementById('Kaart');
        element.parentNode.removeChild(element);
        }
        const enemy = this.currentRoom.hasEnemy;
        const EnemyStats = document.createElement('div');
        EnemyStats.className = 'enemystats';
        EnemyStats.id = 'enemystats';
        this._el.appendChild(EnemyStats);
        EnemyStats.innerHTML = '<img src=' + enemy.Image + '></img></br>' + enemy.Name + '</br>' + enemy.CurrentHealth + '/' + enemy.MaxHealth + 
        ' HP</br>' + enemy.Damage + ' DMG</br>' + enemy.Armor + ' Armor</br>' + enemy.Evasion + '% Evasion';
        this.eStatsUp = true;
    }

    private loop()
    { //some sort of looping to keep on playing
        const time = this.randomTime(200, 1000);
        if(this.eStatsUp == false)   {
            if(this.currentRoom.hasEnemy != null)   {
                this.EnemyStats(); 
                var element1 = document.getElementById("North");
                element1.parentNode.removeChild(element1); 
                var element1 = document.getElementById("East");
                element1.parentNode.removeChild(element1);
                var element2 = document.getElementById("South");
                element2.parentNode.removeChild(element2);
                var element3 = document.getElementById("West");
                element3.parentNode.removeChild(element3);
                var element4 = document.getElementById("Rest");
                element4.parentNode.removeChild(element4);
                new AttackCommand(this, 'Attack');
                this.eStatsUp = true;  }
        }
        else{
            if(this.currentRoom.hasEnemy == null)   {
                let element = document.getElementById("enemystats");
                element.parentNode.removeChild(element);
                this.eStatsUp = false;  }
        }
        if(this.player.CurrentHealth <= 0)
        {
            this.gameOver();
        }

        setTimeout(() => { //you need to use arrow notation to keep the this keyword into scope
            if(!this._timeUp) this.loop();
        }, time);
    }

}