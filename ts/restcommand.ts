class RestCommand extends AdaptCommand
{
    constructor(game : Game, rest : string)
    {
        super(game, rest)
    }

    execute()
    {
        if(this.game.currentRoom.burn == true)
        {
            this.game.out.println('You cannot rest in this burning room.');
        }
        else
        {
        if(this.game.player.CurrentHealth < this.game.player.MaxHealth)
            {
            this.game.player.CurrentHealth = this.game.player.MaxHealth;
            let pelement = document.getElementById("playerstats");
            pelement.parentNode.removeChild(pelement);
            this.game.PlayerStats();
            }
            else
            {
            this.game.out.println('You are well rested.')
            }
        }
    }
}