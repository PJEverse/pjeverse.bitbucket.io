class Character
{
    MaxHealth : number;
    CurrentHealth : number;
    Damage : number;
    Armor : number;
    Evasion : number;
    Gold : number;

    constructor(MaxHealth : number, Damage : number, Armor : number, Evasion : number, Gold : number)
    {
        this.MaxHealth = MaxHealth;
        this.CurrentHealth = MaxHealth;
        this.Damage = Damage;
        this.Armor = Armor;
        this.Evasion = Evasion;
        this.Gold = Gold;
    }
    TakeDamage(Player : Character, Enemy : Enemy)  : string 
    {
        if(Math.random() * 100 > Player.Evasion)
        {
            if((Enemy.Damage - Player.Armor) < 5)
            {
                Player.CurrentHealth = Player.CurrentHealth - 5;
                return Enemy.Name + ' did 5 damage to you.';
            }
                else
                {
                Player.CurrentHealth = Player.CurrentHealth - (Enemy.Damage - Player.Armor);
                return Enemy.Name + ' did ' + Enemy.Damage + ' damage to you. ' + 'Your armor blocked ' + Player.Armor + ' Damage.';
            }
               
        }
        else
        {
            return 'You avoided the attack.';
        }
    }
        DealDamage(Player : Character, Enemy : Enemy)  : string
        {
            if(Math.random() * 100 > Enemy.Evasion)
            {
                if((Player.Damage - Enemy.Armor) < 5)
                {
                    Enemy.CurrentHealth = Enemy.CurrentHealth - 5;
                    return 'You did 5 Damage.';
                }
                else
                {
                Enemy.CurrentHealth = Enemy.CurrentHealth - (Player.Damage - Enemy.Armor);
                return 'You did ' + Player.Damage + ' Damage. Their armor blocked ' + Enemy.Armor + ' damage.';
                }
            }
    else
    {
        return Enemy.Name + ' avoided the attack.';
    }
        }
}