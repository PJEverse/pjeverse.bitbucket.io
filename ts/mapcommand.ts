class MapCommand extends AdaptCommand
{
    execute()
    {
        if(document.getElementById('Kaart') == null)
        {
        const Kaart = document.createElement('div');
        Kaart.id = 'Kaart';
        this.game._el.appendChild(Kaart);
        Kaart.innerHTML = '<img src= img/map.svg></img>';
        }
        else
        {
            var element = document.getElementById("Kaart");
            element.parentNode.removeChild(element); 
        }
    }
}