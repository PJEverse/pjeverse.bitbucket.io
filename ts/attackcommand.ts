class AttackCommand extends AdaptCommand
{
    constructor(game : Game, attack : string)
    {
        super(game, attack)
    }

    execute()
    {
        this.game.out.println(this.game.player.DealDamage(this.game.player, this.game.currentRoom.hasEnemy));
        this.game.out.println('>');
        if(this.game.currentRoom.hasEnemy.CurrentHealth > 0)
        {
        this.game.out.println(this.game.player.TakeDamage(this.game.player, this.game.currentRoom.hasEnemy));
        }
        this.game.out.println('>');
           if(this.game.currentRoom.hasEnemy.burn == true)
                {
                    this.game.player.CurrentHealth = this.game.player.CurrentHealth - 5;
                    this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' set you on fire. It does 5 damage.');
                }
        let element = document.getElementById("enemystats");
        element.parentNode.removeChild(element);
        this.game.EnemyStats();
            if(this.game.player.CurrentHealth > 0)
            {
                if(this.game.currentRoom.hasEnemy.CurrentHealth <= 0)
                {
                    this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' died.');
                    this.game.player.Gold = this.game.currentRoom.hasEnemy.Gold + this.game.player.Gold;
                    this.game.out.println(this.game.currentRoom.hasEnemy.Name + ' dropped ' + this.game.currentRoom.hasEnemy.Gold + ' gold.')
                    if(this.game.currentRoom.hasEnemy.Name == "Deathwing")
                    {
                        this.game.out.println("You have defeated the boss! You Win!");
                        this.game.gameOver();
                    }
                    else
                    {
                    this.game.currentRoom.hasEnemy = null;
                    const buttons = document.getElementById('buttons');
                    let element2 = document.getElementById("Attack");
                    element2.parentNode.removeChild(element2);
                    new GoCommand(this.game, 'North');
                    new GoCommand(this.game, 'East');
                    new GoCommand(this.game, 'South');
                    new GoCommand(this.game, 'West');
                    new RestCommand(this.game, 'Rest');
                    }
                }
            }
        let pelement = document.getElementById("playerstats");
        pelement.parentNode.removeChild(pelement);
        this.game.PlayerStats();
    }
}