class Enemy extends Character 
{
    Name : string;
    Image : string;
    burn : boolean;

    constructor(Name : string, Image : string, MaxHealth : number, Damage : number, Armor : number, Evasion : number, Gold : number, burn : boolean)  
    {
        super(MaxHealth, Damage, Armor, Evasion, Gold);
        this.Name = Name;
        this.Image = Image;
        this.burn = burn;
    }
}