abstract class AdaptCommand  {
    button : string;
    game : Game;

    constructor(game : Game, button : string)   
    {
        this.game = game;
        this.render(button);
    }

    render(buttonid : string)    
    {
        let buttons =document.querySelector('#buttons');
        const button = document.createElement('button');
        button.id = buttonid;
        button.innerHTML = buttonid;
        button.addEventListener('click', this.click);
        buttons.appendChild(button);
    }

    execute() : void {
        console.log('NOT IMPLEMENTED!!! PLS DO');
    }

    click = () =>
    {
        this.execute();
    }

}