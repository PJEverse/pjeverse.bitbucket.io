class BuyCommand extends AdaptCommand
{
Item : Item;

    constructor(game : Game, Item : Item)
    {
        super(game, Item.Name);
        this.Item = Item;
    }

    execute()
    {
        if(this.Item.Name == 'Evasion' && this.game.player.Evasion >= 80)
        {
            this.game.out.println("You can't get any faster!")
        }
        else
        {
            if(this.game.player.Gold - this.Item.Gold < 0)
            {
                this.game.out.println("You don't have enough gold.");
            }
            else
            {
            this.game.player.MaxHealth = this.game.player.MaxHealth + this.Item.Health;
            this.game.player.Damage = this.game.player.Damage + this.Item.Damage;
            this.game.player.Armor = this.game.player.Armor + this.Item.Armor;
            this.game.player.Evasion = this.game.player.Evasion + this.Item.Evasion;
            this.game.player.Gold = this.game.player.Gold - this.Item.Gold;
            this.game.player.CurrentHealth = this.game.player.CurrentHealth + this.Item.Health;
            this.game.out.println('You bought a ' + this.Item.Desc);
            let element = document.getElementById("playerstats");
            element.parentNode.removeChild(element);
            this.game.PlayerStats();
            }
        }
    }


render(buttonid : string)    
    {
        let buttons =document.querySelector('#buttons');
        const button = document.createElement('button');
        button.id = 'Item';
        button.innerHTML = buttonid;
        button.addEventListener('click', this.click);
        buttons.appendChild(button);
    }
}