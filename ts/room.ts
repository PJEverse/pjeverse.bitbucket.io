
class Room {
    description : string;
    hasEnemy : Enemy;
    northExit : Room;
    southExit : Room;
    eastExit : Room;
    westExit : Room;
    burn : boolean;

    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    constructor(description : string, burn : boolean) 
    {
        this.description = description;
        this.hasEnemy = null;
        this.burn = burn;
    }

    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    setExits(north : Room, east : Room, south : Room, west : Room) : void 
    {
        if(north != null) {
            this.northExit = north;
        }
        if(east != null) {
            this.eastExit = east;
        }
        if(south != null) {
            this.southExit = south;
        }
        if(west != null) {
            this.westExit = west;
        }
    }

    spawnEnemy = (name : string, img : string, hp : number, dmg : number, ar : number, ev : number, gd : number, burn : boolean) => 
    {
        let enemy = new Enemy(name, img, hp, dmg, ar, ev, gd, burn);
        this.hasEnemy = enemy;
    }
}
