class GoCommand extends AdaptCommand{
    direction : string;

    constructor(game : Game, direction : string) {
        super(game, direction);
        this.direction = direction;
    }

    private getNextRoom() : Room
    {
            switch(this.direction)  {
                case 'North': {
                return this.game.currentRoom.northExit;
                }
                case 'East' : {
                return this.game.currentRoom.eastExit;
                }
                case 'South' : {
                return this.game.currentRoom.southExit;
                }
                case 'West' : {
                return this.game.currentRoom.westExit;
                }
                default :
                return null;

            }

    }

    execute()
    {
        let nextRoom = null;
        nextRoom = this.getNextRoom();
        if (nextRoom == null) {
            this.game.out.println("There is no door!");
        }
        else {
            this.game.currentRoom = nextRoom;
            this.game.out.println("You are " + this.game.currentRoom.description);
                if(this.game.currentRoom.burn == true)
                {
                    this.game.out.println("You are on fire!")
                    this.game.player.CurrentHealth = this.game.player.CurrentHealth - 3;
                    let element = document.getElementById("playerstats");
                    element.parentNode.removeChild(element);
                    this.game.PlayerStats();
                }
            this.game.out.print("Exits: ");
        if(this.game.currentRoom.northExit != null) {
            this.game.out.print("north ");
        }
        if(this.game.currentRoom.eastExit != null) {
            this.game.out.print("east ");
        }
        if(this.game.currentRoom.southExit != null) {
            this.game.out.print("south ");
        }
        if(this.game.currentRoom.westExit != null) {
            this.game.out.print("west ");
        }
        this.game.out.println();
        this.game.out.print(">");
           if(this.game.currentRoom.description == 'at the shop.')
            {
            new ShopCommand(this.game, 'Shop');
            }
            else
            {
                let element = document.getElementById("Shop");
                    if(element != null)
                    {
                    element.parentNode.removeChild(element);
                    }
            }
        }
    }
}