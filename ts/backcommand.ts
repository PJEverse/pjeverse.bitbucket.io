class BackCommand extends AdaptCommand
{
    constructor(game : Game, back : string)
    {
        super(game, back);
    }

    execute()
    {
        let i:number;
        for(i = 0; i < 5; i++)  {
        let element = document.getElementById('Item');
        element.parentNode.removeChild(element);
        }
        let element2 = document.getElementById('Back');
        element2.parentNode.removeChild(element2);
        new GoCommand(this.game, 'North');
        new GoCommand(this.game, 'East');
        new GoCommand(this.game, 'South');
        new GoCommand(this.game, 'West');
        new RestCommand(this.game, 'Rest');
        new ShopCommand(this.game, 'Shop');
    }
}