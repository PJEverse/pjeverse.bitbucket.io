class Item  {
    Name : string;
    Desc : string;
    Health : number;
    Damage : number;
    Armor : number;
    Evasion : number;
    Gold : number;

    constructor(Name : string, Desc : string, Health : number, Damage : number, Armor : number, Evasion : number, Gold : number)
    {
        this.Name = Name;
        this.Desc = Desc;
        this.Health = Health;
        this.Damage = Damage;
        this.Armor = Armor;
        this.Evasion = Evasion;
        this.Gold = Gold;
    }

}